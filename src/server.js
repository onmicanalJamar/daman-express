const express = require('express');
const bodyParser =  require("body-parser");
const routes = require('./network/routes');
const compression = require('compression');
const path = require('path');
require('dotenv').config();
const port = 3000;
const app = express();

//seting
var allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

  // intercept OPTIONS method
  if ('OPTIONS' == req.method) {
    res.send(200);
  }
  else {
    next();
  }
};
app.set('views', path.join(__dirname, 'views') );
app.set('view engine', 'ejs');

// middlewares
app.use(allowCrossDomain);
app.use(bodyParser.json({ type: 'application/*+json' }));
app.use(bodyParser.raw({ type: 'application/vnd.custom-type' }));
app.use(bodyParser.text({ type: 'text/html' }))
app.use(bodyParser.json());

//routes
routes(app);


// Static
app.use(express.static(path.join(__dirname, 'public')));
//app.use('/app', express.static('public'));

app.listen(port, ()=> console.log(`serve on port ${port}`));
