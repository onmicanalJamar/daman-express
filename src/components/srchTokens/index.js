const express = require('express');
const router = express.Router();
const fetch = require('node-fetch');

const getOrder = async (token) => {

    try {
        let order = await fetch(`https://5wu4fvg0sl.execute-api.us-east-1.amazonaws.com/Prod/${token}`).then(data =>  data.json()).then(data => {
            return JSON.parse(data.note);
        })
        return order
    } catch (error) {
        return null
    }
}

router.post('/', async (req, res) => {
    let {token} = req.body;
    try{
        let data = await getOrder(token);

        res.json({
            status: true,
            data: data
        })
    }catch(err){
        res.json({
            status: false,
            data: 'no hay orden'
        })
    }
});

module.exports = router;