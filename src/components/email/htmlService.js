const htmlCLient = (name, tel, data) => {

    var firstName = name.replace(/ .*/,'');
    var data = `
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>¡Hemos recibido tu solicitud!</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body style="margin: 0; padding: 0;">
        <table border="0" cell padding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
                        <tr>
                            <td style="padding: 1em;">
                                <img src="https://cdn.shopify.com/s/files/1/0070/7323/5057/email_settings/logo_jamar_rojo.png" alt="Muebles Jamar" width="150" height="auto" style="display: block;" />
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
                                            <b>¡Hemos recibido tu solicitud!</b>
                                        </td>
                                    </tr>
                                    <tr>
                                    <td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                                        Hola ${firstName}, gracias por comunicarte con nosotros, estamos para ayudarte en lo que necesites. En breve recibirás respuesta a tu solicitud.

                                    </tr>
                                </td>
                                <tr>
                                <td align="center" style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                                  <b>¡Bienvenido a nuestra familia Jamar!</b> </td>
                                </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#f5f3f3" style="padding: 30px 30px 30px 30px;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td style="color: #5e5e5e; font-family: Arial, sans-serif; font-size: 14px;" width="60%">
                                            &reg; Muebles Jamar 2020<br/>
                                            </td>
                                        <td align="right" width="40%">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                <a href="https://instagram.com/mueblesjamar"><img style="width: 30px;padding: 4px;"class="img-redes" src="https://cdn.shopify.com/s/files/1/0070/7323/5057/t/18/assets/instagram.png?v=3208155501915401386"></a>
    <a href="https://www.facebook.com/MueblesJamar"><img style="width: 30px;padding: 4px;"class="img-redes" src="https://cdn.shopify.com/s/files/1/0070/7323/5057/t/18/assets/facebook.png?v=12760114208469826219"></a>
    <a href="https://twitter.com/mueblesjamar"><img style="width: 30px;padding: 4px;"class="img-redes" src="https://cdn.shopify.com/s/files/1/0070/7323/5057/t/18/assets/twitter.png?v=16382426690607317072"></a>
    <a href="https://www.youtube.com/user/MueblesJamar"><img style="width: 30px;padding: 4px;"class="img-redes" src="https://cdn.shopify.com/s/files/1/0070/7323/5057/t/18/assets/youtobe.png?v=3293200176601122130"></a>
    <a href="#"><img style="width: 30px;padding: 4px;"class="img-redes" src="https://cdn.shopify.com/s/files/1/0070/7323/5057/t/18/assets/pinters.png?v=1814187480486857641"></a>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
    </html>
    `;
    return data

}
module.exports = { htmlCLient }