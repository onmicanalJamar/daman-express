const express = require('express');
const router = express.Router();
const connectDB = require('../../db');

router.post('/', async (req, res) => {

    let user;
    const {id, email, phone} = req.body;
    try {
        db = await connectDB();
        user = id ? await db.collection('USERS').findOne({ NUMERO_IDENTIFICACION: id }) :
            email ? await db.collection('USERS').findOne({ DIR_ELECT:  email.toUpperCase() }) : 
            await db.collection('USERS').findOne({ NUMERO_CELULAR :  phone });
        res.json({
            status: true,
            data: user
        })
    } catch (error) {
        res.json({
            status: false,
            data: "No se encontró el usuario"
        })
    } 
});

module.exports = router;