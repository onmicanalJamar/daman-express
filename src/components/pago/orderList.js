const express = require('express');
const router = express.Router();
const connectDB = require('../../db');

router.get('/', async (req, res) => {
    let db, listOP;
    try {
        db = await connectDB();
        listOP = await db.collection('OP').find().toArray();
        res.json({
            status: true,
            data: listOP
        });
    }catch(err){
        res.json({
            status: false,
        });
    };
});


module.exports = router;