const express = require('express');
const router = express.Router();
const crypto = require('crypto');
const getRawBody = require('raw-body');
const connectDB = require('../../db');

router.post('create', async (req, res) => {
    res.send('OK');
    let db = await connect();
    await db.collection('OP').insertOne(req.body);
})

router.post('update', async (req, res) => {
    const theData = req.body;

    const {token, customer} = theData;

    res.send('OK');

    let db = await connect();
    let orden = await db.collection('OP').findOne({ token: token });

    if(orden){
        console.log('exite');
        console.log(theData);
        if(!orden.customer){

            if(theData.customer){
                await db.collection('OP').updateOne(
                    { token: token },
                    { $set: { customer: { ...theData.customer }  } }
                );
            }
        }
        if(!orden.shipping_address){
            if(theData.shipping_address){
                await db.collection('OP').updateOne(
                    { token: token },
                    { $set: { shipping_address: { ...theData.shipping_address }  } }
                );
            }
        }
    }
});


module.exports = router;