const express = require('express');
const router = express.Router();
const fetch = require('node-fetch');

const connectDB = require('../../db');

function removeDuplicates(originalArray, prop) {
    var newArray = [];
    var lookupObject  = {};

    for(var i in originalArray) {
       lookupObject[originalArray[i][prop]] = originalArray[i];
    }

    for(i in lookupObject) {
        newArray.push(lookupObject[i]);
    }
     return newArray;
}
const reducer = (numero) => {
    let converte = numero.toString();
    var finalnumer="";
    for(let i = 0; i <= converte.length -3; i++ ){
        finalnumer += converte[i];
    }
    return parseInt(finalnumer)
}
const agencias = [
    {
        name:  "SANTANDER", value: 18 , code: "SD"
    },
    {
        name:  "BOLIVAR", value: 67 , code: "BO",
    },
    {
        name:  "MAGDALENA", value: 85 , code: "MA"
    },
    {
        name:  "CESAR", value: 45 , code: "CE"
    },
    {
        name:  "CORDOBA", value: 55 , code: "CO"
    },
    {
        name:  "GUAJIRA", value: 39 , code: "GU"
    },
    {
        name:  "SUCRE", value: 65 , code: "SU"
    },
    {
        name:  "ANTIOQUIA", value: "A3" , code: "AN"
    },
    {
        name:  "CUNDINAMARCA", value: "A9" , code: "CU"
    },
    {
        name:  "ATLANTICO", value: "01" , code:  "AT"
    },
];
let tasasSeguros = [
    {
      "oro": true,
      "CUOTAS": 6,
      "TASA_EFECTIVA_ANUAL": 0.00,
      "TASA_NOMINAL_MENSUAL": 0.00,
      "VALOR_SEGURO_POR_MILLON": 0
    },
    {
      "oro": true,
      "CUOTAS": 10,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 0
    },
    {
      "oro": true,
      "CUOTAS": 12,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 0
    },
    {
      "oro": true,
      "CUOTAS": 13,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 0
    },
    {
      "oro": true,
      "CUOTAS": 20,
      "TASA_EFECTIVA_ANUAL": 19.70,
      "TASA_NOMINAL_MENSUAL": 1.50977049,
      "VALOR_SEGURO_POR_MILLON": 4800
    },
    {
      "oro": true,
      "CUOTAS": 22,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 10800
    },
    {
      "oro": true,
      "CUOTAS": 23,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 9219
    },
    {
      "oro": true,
      "CUOTAS": 24,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 9221
    },
    {
      "oro": true,
      "CUOTAS": 27,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 9223
    },
    {
      "oro": true,
      "CUOTAS": 30,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 9426
    },
    {
      "oro": true,
      "CUOTAS": 34,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 9430
    },
    {
      "oro": true,
      "CUOTAS": 37,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 9200
    },
    {
      "oro": true,
      "CUOTAS": 44,
      "TASA_EFECTIVA_ANUAL": 23.53,
      "TASA_NOMINAL_MENSUAL": 1.77654491,
      "VALOR_SEGURO_POR_MILLON": 10300
    },
    {
      "oro": false,
      "CUOTAS": 6,
      "TASA_EFECTIVA_ANUAL": "27,80",
      "TASA_NOMINAL_MENSUAL": "2,06517185",
      "VALOR_SEGURO_POR_MILLON": 0
    },
    {
      "oro": false,
      "CUOTAS": 10,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 0
    },
    {
      "oro": false,
      "CUOTAS": 12,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 0
    },
    {
      "oro": false,
      "CUOTAS": 13,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 0
    },
    {
      "oro": false,
      "CUOTAS": 16,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 5014
    },
    {
      "oro": false,
      "CUOTAS": 22,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 10800
    },
    {
      "oro": false,
      "CUOTAS": 23,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 9219
    },
    {
      "oro": false,
      "CUOTAS": 24,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 9221
    },
    {
      "oro": false,
      "CUOTAS": 27,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 9223
    },
    {
      "oro": false,
      "CUOTAS": 30,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 206517185,
      "VALOR_SEGURO_POR_MILLON": 9426
    },
    {
      "oro": false,
      "CUOTAS": 34,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 9430
    },
    {
      "oro": false,
      "CUOTAS": 37,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 9200
    },
    {
      "oro": false,
      "CUOTAS": 40,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 10536
    }
];

const productBackenJamar = async (sku, region) => {
    let product = fetch(`https://backend.jamar.co/record_products/NFF/${region ? region : "ATLANTICO" }/review_variants/${sku}`)
    .then(data => data.json()).then(data => data);
    return product
};
const productShopifyJamar = async (sku, region) => {
    let product = fetch(`https://www.jamar.com/search?type=product&q=variants.sku:${sku}&view=sku`)
    .then(data => data.json()).then(data => data);
    return product
}
const getCuota = async (sku, region, type) => {
    let cuota = fetch(`https://ihoffgzow6.execute-api.us-east-1.amazonaws.com/api/v1/feesProducts/JA/${region ? region :"01"}?sku='${sku}'&tven='${type}'&tipoGrupo='A','G'`)
        .then(data => data.json()).then(data => data);
    return cuota
}
const getOrder = async (token) => {

    try {
        let order = await fetch(`https://5wu4fvg0sl.execute-api.us-east-1.amazonaws.com/Prod/${token}`).then(data => data.json())
        return order
    } catch (error) {
        return null
    }
}
router.post('/search', async (req, res) => {
    const {orderId, tokenCheck, gold } = req.body;
    try {

        let op = await getOrder(tokenCheck);

        const {line_items, note} = op;

        var productoInfo = [];
        var productTitles= [];
        var cuotasProduc = [];
        let notaOp = JSON.parse(note)
        let typeClien = gold == true ? "OR" : "TJ";
        //console.log(notaOp.fee)
        //buscando el departamento
        let departament = agencias.filter( iten => iten.code == notaOp.department )

        //sacabdo los porductos del carrito de compras
        for(let iten = 0; iten <= line_items.length -1 ;iten++){
            let dataProduc = await productShopifyJamar(line_items[iten].sku);
            let cuotasLista = await getCuota(line_items[iten].sku, departament[0].value , typeClien);
            productTitles.push(line_items[iten].presentment_variant_title);
            productoInfo.push(dataProduc);
            let cuotasP = {
                sku: line_items[iten].sku,
                fees: cuotasLista.records
            }
            //console.log(cuotasP)
            cuotasProduc.push(cuotasP);
        };

        //sacando cuotas y eliminado cuotas duplicadas
        var cutoList = [];
        cuotasProduc.forEach( (number) => {
            var cuotaP=[];
            number.fees.forEach( (iten, index) => {
                if(gold == true){
                    if(iten.type == "OR"){
                        if(iten.type_group == "G"){
                            cuotaP.push(iten);
                        } else if(iten.type_group == "A"){
                            cuotaP.push(iten);
                        }
                    }
                }
                if(gold == false){
                    if(iten.type == "TJ"){
                        if(iten.type_group == "G"){
                            cuotaP.push(iten);
                        } else if(iten.type_group == "A"){
                            cuotaP.push(iten);
                        }
                    }
                };
            });
            var uniqueArray = removeDuplicates(cuotaP, "dues");

            let product = {
               sku: number.sku,
               fees: uniqueArray
            }
            cutoList.push(product)
        });

        //sumando cuotas
        var cuotasTodo = [];
        var fesP=[];
        if( cutoList.length >= 2 ){
            for(let i = 0; i <= cutoList.length -1; i++ ){

                let fessList = cutoList[i].fees;
                fessList.forEach((el, index) => {
                    if(el.dues != undefined){
                        fesP.push(el);
                    }
                });
            };
            function arasuma(arra, filter){
                let iteradtor = arra.filter(i => i.dues == filter);
                let sumas = {
                    dues: 0,
                    price_without_prom: 0,
                    monthly_fee: 0,
                    monthly_fee_without_promo: 0,
                    regular_price: 0,
                    time_scale: 'S',
                    type: 'OR',
                    sku: '7017320',
                    type_group: 'A'
                };
                let finalFess = {
                    dues: 0,
                    price_without_prom: 0,
                    monthly_fee: 0,
                    type: 'OR',
                }

                iteradtor.forEach ( (numero, index) => {

                    let arrayUnique = line_items.filter(i => i.sku == numero.sku);
                    let quiality = arrayUnique[0].quantity;

                    let final = {
                        dues: numero.dues,
                        price_without_prom: sumas.price_without_prom += numero.price_without_prom,
                        monthly_fee: sumas.monthly_fee += numero.monthly_fee,
                        monthly_fee_without_promo: sumas.monthly_fee_without_promo += numero.monthly_fee_without_promo,
                        regular_price: sumas.regular_price += numero.regular_price,
                        time_scale: 'S',
                        type: numero.type,
                        sku: numero.sku,
                        type_group: numero.type_group
                    };
                    let totalc = final.monthly_fee_without_promo * final.dues;
                    finalFess = {
                        dues: final.dues,
                        price_without_prom: final.price_without_prom ,
                        monthly_fee: (final.monthly_fee_without_promo * quiality),
                        type: numero.type,
                        cupototal: totalc * quiality
                    }
                    return final
                });
                return finalFess
            }

            let result16 = arasuma(fesP, 16);

            cuotasTodo.push(result16);
            let result20 = arasuma(fesP, 20);
            cuotasTodo.push(result20);
            let result24 = arasuma(fesP, 24);
            cuotasTodo.push(result24);
            let result30 = arasuma(fesP, 30);
            cuotasTodo.push(result30);
            /* let result40 = arasuma(fesP, 40);
            cuotasTodo.push(result40); */

        }else{
            for(let i = 0; i <= cutoList.length -1; i++ ){
                let fessList = cutoList[i].fees;
                for(let iten = 0; iten <= fessList.length -1; iten++ ){
                    if(fessList[iten].dues){
                        if(fessList[iten].dues != 40){
                            let finalCuota = {
                                dues: fessList[iten].dues ,
                                price_without_prom : fessList[iten].price_without_prom + notaOp.fee ,
                                monthly_fee : fessList[iten].monthly_fee_without_promo,
                                type: fessList[iten].type,
                                cupototal: fessList[iten].monthly_fee_without_promo * fessList[iten].dues
                                //"type_group": fessList[iten].type_group
                            };
                            cuotasTodo.push(finalCuota);
                        }
                    };
                };
            };
        };

        let productFinal = [];
        var finaltitles =[];

        //Trayendo los titles de los porductos
        for(let iten = 0; iten <= productTitles.length -1 ;iten++){
            let title = productTitles[iten];
            let info = title.split(" ")

            for(i =0; i <= info.length -1; i++){
                if(info[i] == "FF"){
                    info[i] = "NFF";
                };
            };
            let finalTitle = info.join(' ');
            finaltitles.push(finalTitle);
        };

        //buscando el porducto en NFF y Comprando por title
        for(let iten = 0; iten <= productoInfo.length -1 ;iten++){

            if(productoInfo[iten].products.length >= 1){
                let {variants} = productoInfo[iten].products[0];
                let dataPoduct = variants.filter( i => i.title == finaltitles[iten])
                productFinal.push(dataPoduct[0])
            }

        };
        //Sumando precios
        var listPrices = [];
        var suma = 0;

        for(let iten = 0; iten <= productFinal.length -1 ;iten++){

            let { price , sku} = productFinal[iten];
            let finalPrice = reducer(price);
            let finalMultiplos = 0;
            line_items.forEach(i => {
                if(i.sku == sku) {
                    finalMultiplos = finalPrice * i.quantity
                }
            })
            //console.log(finals);
            listPrices.push(finalMultiplos);
        };

        listPrices.forEach( (numero) => {
            suma += numero;
        });

        const type = gold ? "OR" : "TJ";

        let transport = notaOp.fee;
        let productsTotals = [];
        op.line_items.forEach(element => {
            let objs = {
                sku: element.sku,
                quantity: element.quantity,
                name: element.title
            }
            productsTotals.push(objs);
        });
        let fesultimate = [];
        cuotasTodo.forEach( async (i) => {
            let obj = {
                dues: i.dues,
                price_without_prom: i.price_without_prom,
                monthly_fee: i.monthly_fee,
                type: i.type,
                cupototal: i.cupototal
            };
            if(notaOp.fee >= 1){
                await fetch(`https://ihoffgzow6.execute-api.us-east-1.amazonaws.com/api/v1/pricesProducts/JA/01?capital=${notaOp.fee}&tipoventa=OR&numcuotas=${i.dues}`)
                .then(data => data.json() )
                .then(data => {
                    obj.monthly_fee = obj.monthly_fee + data.data[0].valorcuotas;
                    obj.cupototal =  obj.cupototal + data.data[0].valorcuotas;
                });
            }
            fesultimate.push(obj);
        });

        if(suma == 0){
            res.json({
                status: false,
                dataOp: "producto no disponible"
            });
        }else{
            res.json({
                status: true,
                totalPrice: {
                    transport: transport,
                    subtotal: suma,
                    total: suma + transport
                },
                orderid: `WJA-${op.id}`,
                storeCode: "C1",
                products: productsTotals,
                seller: "VENDEDOR E-COMMERCE",
                shippingData: {
                    department: notaOp.department_name,
                    city: notaOp.city_name,
                    neighborhood: notaOp.barrios ? notaOp.barrios : notaOp.neighborhood,
                    address: op.shipping_address.address1
                },
                fess: fesultimate
            });
        }
    }
    catch(err){
        console.log(err);
        res.json({
            status: false,
            dataOp: "no existe esta OPenShopify"
        });
    };
});

module.exports = router;