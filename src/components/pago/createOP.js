const express = require('express');
const router = express.Router();
const fetch = require('node-fetch');
const connectDB = require('../../db');
var Finance = require('financejs');
var amortize = require('amortize');

var finance = new Finance();
const agencias = [
    {
        name:  "SANTANDER", value: 18 , code: "SD"
    },
    {
        name:  "BOLIVAR", value: 67 , code: "BO",
    },
    {
        name:  "MAGDALENA", value: 85 , code: "MA"
    },
    {
        name:  "CESAR", value: 45 , code: "CE"
    },
    {
        name:  "CORDOBA", value: 55 , code: "CO"
    },
    {
        name:  "GUAJIRA", value: 39 , code: "GU"
    },
    {
        name:  "SUCRE", value: 65 , code: "SU"
    },
    {
        name:  "ANTIOQUIA", value: "A3" , code: "AN"
    },
    {
        name:  "CUNDINAMARCA", value: "A9" , code: "CU"
    },
    {
        name:  "ATLANTICO", value: "01" , code:  "AT"
    },
];
let tasasSeguros = [
    {
      "oro": true,
      "CUOTAS": 6,
      "TASA_EFECTIVA_ANUAL": 0.00,
      "TASA_NOMINAL_MENSUAL": 0.00,
      "VALOR_SEGURO_POR_MILLON": 0
    },
    {
      "oro": true,
      "CUOTAS": 10,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 0
    },
    {
      "oro": true,
      "CUOTAS": 12,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 0
    },
    {
      "oro": true,
      "CUOTAS": 13,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 0
    },
    {
      "oro": true,
      "CUOTAS": 20,
      "TASA_EFECTIVA_ANUAL": 19.70,
      "TASA_NOMINAL_MENSUAL": 1.50977049,
      "VALOR_SEGURO_POR_MILLON": 4800
    },
    {
      "oro": true,
      "CUOTAS": 22,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 10800
    },
    {
      "oro": true,
      "CUOTAS": 23,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 9219
    },
    {
      "oro": true,
      "CUOTAS": 24,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 9221
    },
    {
      "oro": true,
      "CUOTAS": 27,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 9223
    },
    {
      "oro": true,
      "CUOTAS": 30,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 9426
    },
    {
      "oro": true,
      "CUOTAS": 34,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 9430
    },
    {
      "oro": true,
      "CUOTAS": 37,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 9200
    },
    {
      "oro": true,
      "CUOTAS": 44,
      "TASA_EFECTIVA_ANUAL": 23.53,
      "TASA_NOMINAL_MENSUAL": 1.77654491,
      "VALOR_SEGURO_POR_MILLON": 10300
    },
    {
      "oro": false,
      "CUOTAS": 6,
      "TASA_EFECTIVA_ANUAL": "27,80",
      "TASA_NOMINAL_MENSUAL": "2,06517185",
      "VALOR_SEGURO_POR_MILLON": 0
    },
    {
      "oro": false,
      "CUOTAS": 10,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 0
    },
    {
      "oro": false,
      "CUOTAS": 12,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 0
    },
    {
      "oro": false,
      "CUOTAS": 13,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 0
    },
    {
      "oro": false,
      "CUOTAS": 16,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 5014
    },
    {
      "oro": false,
      "CUOTAS": 22,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 10800
    },
    {
      "oro": false,
      "CUOTAS": 23,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 9219
    },
    {
      "oro": false,
      "CUOTAS": 24,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 9221
    },
    {
      "oro": false,
      "CUOTAS": 27,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 9223
    },
    {
      "oro": false,
      "CUOTAS": 30,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 206517185,
      "VALOR_SEGURO_POR_MILLON": 9426
    },
    {
      "oro": false,
      "CUOTAS": 34,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 9430
    },
    {
      "oro": false,
      "CUOTAS": 37,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 9200
    },
    {
      "oro": false,
      "CUOTAS": 40,
      "TASA_EFECTIVA_ANUAL": 27.80,
      "TASA_NOMINAL_MENSUAL": 2.06517185,
      "VALOR_SEGURO_POR_MILLON": 10536
    }
];

let DevAPI = "https://292608693b12d849e6fb6639fe28b150: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@pruebasjamar.myshopify.com/admin/api/2020-04/orders.json";
let PRapi = "https://12c190a9050e9791434bc03dae05ecc9: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@jamar-ecommerce.myshopify.com/admin/api/2020-04/orders.json";

function removeDuplicates(originalArray, prop) {
    var newArray = [];
    var lookupObject  = {};

    for(var i in originalArray) {
       lookupObject[originalArray[i][prop]] = originalArray[i];
    }

    for(i in lookupObject) {
        newArray.push(lookupObject[i]);
    }
     return newArray;
}
const reducer = (numero) => {
    let converte = numero.toString();
    var finalnumer="";
    for(let i = 0; i <= converte.length -3; i++ ){
        finalnumer += converte[i];
    }
    return parseInt(finalnumer)
}
const createOp = async (datos) => {
     let peticion = await fetch(DevAPI, {
        method: 'POST',
        headers : { 'Content-Type': 'application/json' },
        body: JSON.stringify(datos),
    }).then(data => data.json()).then(data => data);
    return peticion;
};
const productShopifyJamar = async (sku, region) => {
    let product = fetch(`https://www.jamar.com/search?type=product&q=variants.sku:${sku}&view=sku`)
    .then(data => data.json()).then(data => data);
    return product
};
const getCuota = async (sku, region, type) => {
    let cuota = fetch(`https://ihoffgzow6.execute-api.us-east-1.amazonaws.com/api/v1/feesProducts/JA/${region ? region :"01"}?sku='${sku}'&tven='${type ? type : "OR"}'&tipoGrupo='A','G'`)
        .then(data => data.json()).then(data => data);
    return cuota
}
const creteOpMec = async ( op,  datos, suma, selecuota, productFinal, cutoList, cc, calculoTranporte ) => {

    let telefono = op.shipping_address ? op.shipping_address.phone: datos.customer ? datos.customer.phone: null;
    var product = [];

    const { fee, date, department, city, department_name, city_name, neighborhood } = JSON.parse(op.note);

    for (let i = 0; i <= productFinal.length -1 ; i++ ){
        let { line_items }  = datos.order;
        let {sku, price} = productFinal[i];
        let fessuniques = cutoList[i].fees;
        let mycuota = fessuniques.filter(iten => iten.dues == selecuota[0].dues);

        let sequenceIten = i + 1;
        let mutiplo = sequenceIten * 10;

        let productOne = {
                "bonus_code": "",
                "discount": 0,
                "dues": mycuota[0].dues,
                "father": 0,
                "fee_value": mycuota[0].monthly_fee_without_promo,
                "gifts": [],
                "promotion_code": "",
                "quantity": line_items[i].quantity ,
                "regular_value": mycuota[0].regular_price,
                "sequence": mutiplo,
                "sku": sku,
                "unit_value": mycuota[0].regular_price
        };
        product.push(productOne);
    }
    let dispatch;
    let client = {};
    if(op.shipping_address){
        const {phone, zip, address1, address2} = op.shipping_address;

        dispatch = {
            "address": address1 ? address1 : "",
            "city_code": city,
            "indications": address2 ? address2 : "",
            "neighborhood_code": neighborhood ? neighborhood : "",
            "phone": telefono,
            "state_code": department ? department : "",
            "zone_code": "00"
        }
    }else{
        dispatch = {
            "address": "",
            "city_code": city,
            "indications": "",
            "neighborhood_code": neighborhood,
            "phone": telefono,
            "state_code": department,
            "zone_code": "00"
        }
    }
    if(op.customer){
        const {note, phone, first_name, last_name, email} = op.customer;
        client = {
            "address": op.shipping_address ? op.shipping_address.address1 : null,
            "cellphone": telefono,
            "cellphone1": null,
            "cellphone2": null,
            "city_code": city ? city : null,
            "email": email ? email : null,
            "first_name": first_name ? first_name : op.shipping_address ? op.shipping_address.first_name : null,
            "identification": cc.toString(),
            "neighborhood_code": neighborhood ? neighborhood : null,
            "second_name": "",
            "second_surname": "",
            "state_code": department ? department : null,
            "surname": last_name ? last_name : op.shipping_address.last_name ? op.shipping_address.last_name: null
        }
    } else{
        client = {
            "address": op.shipping_address ? op.shipping_address : null,
            "cellphone": telefono,
            "cellphone1": null,
            "cellphone2": null,
            "city_code": city ? city : null,
            "email":op.email ? op.email: null,
            "first_name": op.shipping_address ? op.shipping_address.first_name : null,
            "identification": cc.toString(),
            "neighborhood_code": neighborhood ? neighborhood : null,
            "second_name": "",
            "second_surname": "",
            "state_code": department ? department : null,
            "surname": op.shipping_address.last_name ? op.shipping_address.last_name: null
        }
    }

    let dataOrden= {
        "order": {
            "agency_code": "C1",
            "client_takes": "N",
            "client_type": selecuota[0].type,
            "delivery_date": date,
            "dues": selecuota[0].dues,
            "edit": "N",
            "fee_value": selecuota[0].monthly_fee,
            "initial_fee": 0,
            "original": null,
            "quote": "N",
            "seller_code": "800",
            "separate_invoices": "S",
            "session_user": "",
            "t_plan": "N",
            "user_app": "",
            "value": suma,
            "observation": `IDCRE=${op.id}`
        },
        "client": client,
        "products": product,
        "services": [{
            "service_code": "01",
            "service_name": "DELIVERY",
            "service_price": fee
        }],
        "dispatch": dispatch
    }
//https://l04wzzd9ra.execute-api.us-east-1.amazonaws.com/api/v1/JA/orders-new?type=D&country=01
     let dataFinal = await fetch(`https://gm36tm91y3.execute-api.us-east-1.amazonaws.com/api/v1/JA/orders-new?type=D&country=01`,{
        method: 'POST',
        headers : { 'Content-Type': 'application/json' },
        body: JSON.stringify(dataOrden),
    }).then(data => data.json()).then(data=> data);

    return dataFinal

    //return dataOrden;

};
const verify = async (number) => {
        let devUrl = `https://l04wzzd9ra.execute-api.us-east-1.amazonaws.com/api/v1/JA/orders-queue/${number.toString()}`;
        console.log(devUrl);

        let final= await fetch(devUrl).then(data => data.json()).then(data => data); 
        console.log(final);
        return final
}
const transportCalculate = async (transport, fee)=> {
    let typeCustomer = fee.type;
    let pe_nuCapital = transport ? transport: 40000 ;
    let pe_nuCuotas = fee.dues ? fee.dues : 44;

    let tasaSelect = tasasSeguros.filter(iten => {

        if(typeCustomer == 'OR' ){
            if(iten.oro == true){
                return iten.CUOTAS == pe_nuCuotas
            }
        }else if(typeCustomer == 'OR'){
            if(iten.oro == false){
                return iten.CUOTAS == pe_nuCuotas
            }
        } 
    });

    let pe_nuTasa  = parseFloat(tasaSelect[0].TASA_NOMINAL_MENSUAL);
    let pe_nuSeguro = tasaSelect[0].VALOR_SEGURO_POR_MILLON;
    let ps_nuPagoCuota;

    //Sacando el seguro
    let ps_nuTotalSeguro = Math.round ( (pe_nuSeguro / 1000000 * pe_nuCapital) * pe_nuCuotas);

    if(ps_nuTotalSeguro == 0){
        return  transport
    }else{
        //Calculando cuota sin seguro
        ps_nuPagoCuota = ( (pe_nuTasa/100) *  Math.pow( ( 1+(pe_nuTasa/100)) , pe_nuCuotas ) ) * pe_nuCapital / ((  Math.pow((1+(pe_nuTasa/100)), pe_nuCuotas)) -1 ) ;
        console.log(ps_nuPagoCuota);
        //calculando intereses
        let  ps_nuTotalInteres = Math.round((ps_nuPagoCuota * pe_nuCuotas) - pe_nuCapital);

        //nominal con seguro
        let ps_nuTotFinanciadoConSeguro = pe_nuCapital + ps_nuTotalInteres + ps_nuTotalSeguro;

        //valor final
        let finalvalue = ps_nuTotFinanciadoConSeguro / pe_nuCuotas;

        console.log(finalvalue)
        return  Math.round(finalvalue)
    }
}
const getOrder = async (token) => {
    try {
        let order = await fetch(`https://5wu4fvg0sl.execute-api.us-east-1.amazonaws.com/Prod/${token}`).then(data => data.json())
        return order
    } catch (error) {
        return null
    }
}
router.post('/', async (req, res) => {
    let apiProduct = "https://backend.jamar.co/record_products/NFF/ATLANTICO/review_variants/1953946533951";

    const {orderId, tokenCheck, fees , cc } = req.body;

     try {
        let op = await getOrder(tokenCheck);

        const {line_items, note, customer } = op;
        var productoInfo = [];
        var productTitles= [];
        var cuotasProduc = [];
        let notaOp = JSON.parse(note);
        //console.log(line_items);
        var products = [];
        var resultP = [];
        let transport = notaOp.fee;
         //buscando el departamento
        let departament = agencias.filter( iten => iten.code == notaOp.department )

         //sacabdo los porductos del carrito de compras
        for(let iten = 0; iten <= line_items.length -1 ;iten++){

            let dataProduc = await productShopifyJamar(line_items[iten].sku);

            let cuotasLista = await getCuota(line_items[iten].sku, departament[0].value, fees.type );

            productTitles.push(line_items[iten].presentment_variant_title);
            productoInfo.push(dataProduc);
            let cuotasP = {
                sku: line_items[iten].sku,
                fees: cuotasLista.records
            }
            cuotasProduc.push(cuotasP);
        };
        //sacando cuotas y eliminado cuotas duplicadas
        var cutoList = [];
        const gold = fees.type;

        cuotasProduc.forEach( (number) => {
            var cuotaP=[];
            number.fees.forEach( (iten, index) => {
                if(gold == "OR"){
                    if(iten.type == "OR"){

                            if(iten.type_group == "G"){
                                cuotaP.push(iten);
                            } else if(iten.type_group == "A"){
                                cuotaP.push(iten);
                            }
                    }
                }
                if(gold == "TJ"){
                    if(iten.type == "TJ"){

                            if(iten.type_group == "G"){

                                cuotaP.push(iten);
                            } else if(iten.type_group == "A"){

                                cuotaP.push(iten);
                            }

                    }
                };
            });
            var uniqueArray = removeDuplicates(cuotaP, "dues");

            let product = {
               sku: number.sku,
               fees: uniqueArray
            }
            cutoList.push(product)
        });
        let calculoTranporte = transport;
        //sumando cuotas
        var cuotasTodo = [];
        var fesP=[];
        if( cutoList.length >= 2 ){
            for(let i = 0; i <= cutoList.length -1; i++ ){

                let fessList = cutoList[i].fees;
                fessList.forEach((el, index) => {
                    if(el.dues != undefined){
                        fesP.push(el);
                    }
                })

            };

            function arasuma(arra, filter){
                let iteradtor = arra.filter(i => i.dues == filter);
                let sumas = {
                    dues: 0,
                    price_without_prom: 0,
                    monthly_fee: 0,
                    monthly_fee_without_promo: 0,
                    regular_price: 0,
                    time_scale: 'S',
                    type: 'OR',
                    sku: '7017320',
                    type_group: 'A'
                };
                let finalFess = {
                    dues: 0,
                    price_without_prom: 0,
                    monthly_fee: 0,
                    type: 'OR',
                }

                iteradtor.forEach (function(numero){
                    let arrayUnique = line_items.filter(i => i.sku == numero.sku);
                    let quiality = arrayUnique[0].quantity;
                    console.log(quiality);
                    let final = {
                        dues: numero.dues,
                        price_without_prom: sumas.price_without_prom += numero.price_without_prom,
                        monthly_fee: sumas.monthly_fee += numero.monthly_fee,
                        monthly_fee_without_promo: sumas.monthly_fee_without_promo += numero.monthly_fee_without_promo,
                        regular_price: sumas.regular_price += numero.regular_price,
                        time_scale: 'S',
                        type: numero.type,
                        sku: numero.sku,
                        type_group: numero.type_group
                    };
                    let totalc =  final.monthly_fee_without_promo * final.dues;

                    finalFess = {
                        dues: final.dues,
                        price_without_prom: final.price_without_prom ,
                        monthly_fee: final.monthly_fee_without_promo * quiality,
                        type: numero.type,
                        cupototal: totalc * quiality
                    }
                    return final
                });
                return finalFess
            }

            let result16 = arasuma(fesP, 16);
            cuotasTodo.push(result16);
            let result20 = arasuma(fesP, 20);
            cuotasTodo.push(result20);
            let result24 = arasuma(fesP, 24);
            cuotasTodo.push(result24);
            let result30 = arasuma(fesP, 30);
            cuotasTodo.push(result30);
            /* let result40 = arasuma(fesP, 40);
            cuotasTodo.push(result40); */

        }else{
            for(let i = 0; i <= cutoList.length -1; i++ ){
                let fessList = cutoList[i].fees;
                for(let iten = 0; iten <= fessList.length -1; iten++ ){
                    if(fessList[iten].dues){
                        let finalCuota = {
                            "dues": fessList[iten].dues,
                            "price_without_prom": fessList[iten].price_without_prom + notaOp.fee ,
                            "monthly_fee": fessList[iten].monthly_fee_without_promo,
                            "type":  fessList[iten].type,
                            //"type_group": fessList[iten].type_group
                        };

                        cuotasTodo.push(finalCuota);
                    };
                };
            };
        };

        let productFinal = [];
        var finaltitles =[];

        //Trayendo los titles de los porductos
        for(let iten = 0; iten <= productTitles.length -1 ;iten++){
            let title = productTitles[iten];
            let info = title.split(" ")

            for(i =0; i <= info.length -1; i++){
                if(info[i] == "FF"){
                    info[i] = "NFF";
                };
            };
            let finalTitle = info.join(' ');
            finaltitles.push(finalTitle);
        };

        //buscando el porducto en NFF y Comprando por title
        if(productoInfo.length){
            for(let iten = 0; iten <= productoInfo.length -1 ;iten++){

                if(productoInfo[iten].products.length >= 1){
                    let {variants} = productoInfo[iten].products[0];
                    let dataPoduct = variants.filter( i => i.title == finaltitles[iten])
                    productFinal.push(dataPoduct[0])
                }
            };
        }

        //Sumando precios
        var listPrices = [];

        var suma = 0;
        if(productFinal){
            for(let iten = 0; iten <= productFinal.length -1 ;iten++){
                let { price } = productFinal[iten];
                let final =reducer(price);
                listPrices.push(final);
            };
        }

        listPrices.forEach( (numero) => {
            suma += numero;
        });

        // calculando transporte

        let selecuota = cuotasTodo.filter( (iten) => iten.dues == fees.dues );
        var lineItens= [];

        for (let i = 0; i <= productFinal.length -1; i++){
            let dataProduct = {
                "applied_discounts": [],
                //"key": "9c4b59cf76963759e93c2a484dd348ce",
                "destination_location_id": null,
                "fulfillment_service": "manual",
                "gift_card": false,
                "grams": 0,
               // "origin_location_id": 687403794495,
                "presentment_title": productFinal[i].title,
                "presentment_variant_title": productFinal[i].option1 ,
                "product_id": productFinal[i].id ? productFinal[i].id : "no hay",
                "properties": [
                    {
                        "name": "_shipping_price",
                        "value": "0"
                    }
                ],
                "quantity": line_items[i].quantity,
                "requires_shipping": true,
                "sku": productFinal[i].sku,
                "title": productFinal[i].title,
                "variant_id": productFinal[i].id,
                "variant_title": productFinal[i].option1,
                "variant_price":reducer(productFinal[i].price),
                "vendor": productoInfo[i].products[0].vendor,
                "user_id": null,
                "unit_price_measurement": {
                    "measured_type": null,
                    "quantity_value": null,
                    "quantity_unit": null,
                    "reference_value": null,
                    "reference_unit": null
                },
                "country_hs_codes": [],
                "country_code_of_origin": null,
                "province_code_of_origin": null,
                "harmonized_system_code": null,
                "compare_at_price": null,
                "line_price": reducer(productFinal[i].price),
                "price": suma,
                //"id": "9c4b59cf76963759e93c2a484dd348ce"
            }
            lineItens.push(dataProduct);
        };
        let shipping_address = op.shipping_address ?  op.shipping_address: "";

        var datos = {};

        if(customer !== undefined ){
            let customerdata;
            if(Object.keys(customer) == 0){
                customerdata = {
                    "id": customer.id ? customer.id : null,
                    "email": customer.email ? customer.email  : null,
                    "accepts_marketing": true,
                    "created_at": new Date().toDateString(),
                    "updated_at": new Date().toDateString(),
                    "first_name": customer.first_name ? customer.first_name : null,
                    "last_name": customer.last_name ? customer.last_name : op.shipping_address.last_name ? op.shipping_address.last_name : null,
                    "orders_count": 0,
                    "state": "enabled",
                    "total_spent": "0.00",
                    "last_order_id": null,
                    "note": cc,
                    "verified_email": false,
                    "multipass_identifier": null,
                    "tax_exempt": false,
                    "phone": customer.phone ? customer.phone : op.shipping_address.phone ? op.shipping_address.phone :null,
                    "tags": customer.tags ? customer.tags : null,
                    "last_order_name": null,
                    "currency": "COP",
                    "accepts_marketing_updated_at": "2020-01-31T08:19:22-05:00",
                    "marketing_opt_in_level": "single_opt_in",
                    "tax_exemptions": [],
                    "admin_graphql_api_id": "gid://shopify/Customer/2874538197055"
                };
            }else{
                customerdata =  {
                    "id": customer.id ? customer.id : null,
                    "email": customer.email ? customer.email  : null,
                    "accepts_marketing": true,
                    "created_at": "2020-01-31T08:19:21-05:00",
                    "updated_at": "2020-04-13T15:46:56-05:00",
                    "first_name": customer.first_name ? customer.first_name : null,
                    "last_name": customer.last_name ? customer.last_name : null,
                    "orders_count": 0,
                    "state": "enabled",
                    "total_spent": "0.00",
                    "last_order_id": null,
                    "note": cc,
                    "verified_email": false,
                    "multipass_identifier": null,
                    "tax_exempt": false,
                    "phone": customer.phone ? customer.phone : null,
                    "tags": customer.tags ? customer.tags : "",
                    "last_order_name": null,
                    "currency": "COP",
                    "accepts_marketing_updated_at": "2020-01-31T08:19:22-05:00",
                    "marketing_opt_in_level": "single_opt_in",
                    "tax_exemptions": [],
                    "admin_graphql_api_id": "gid://shopify/Customer/2874538197055"
                };
            }

            if(op.shipping_address){
                datos =  {
                    "order": {
                        "line_items": lineItens,
                        "financial_status": "paid",
                        shipping_address,
                        "customer": customerdata
                    }
                };
            }else{
                datos =  {
                    "order": {
                        "line_items": lineItens,
                        "financial_status": "paid",
                        shipping_address,
                        "customer": customerdata
                    }
                };
            }
        }else{
            if(op.shipping_address){
                datos = {
                    "order": {
                        "line_items": lineItens,
                        "financial_status": "paid",
                        shipping_address
                    }
                };
            }else{
                datos = {
                    "order": {
                        "line_items": lineItens,
                        "financial_status": "paid"
                    }
                };
            }
        };

        //Crear OP Shopify
        //console.log(datos)
       let resultsOP = await createOp(datos);

        //crear OP MEC
        let opMec = await creteOpMec(op,  datos, suma, selecuota, productFinal , cutoList, cc, calculoTranporte);
        //let finalInfo = await verify(opMec.num);
         res.json({
            status: true,
            result: resultsOP,
            mecStatus: opMec
            //stateOp: finalInfo
            //transporte: finalTransport
        });

    }catch(er){
        console.log(er);
        res.json({
            status: false,
            message: "error al guardar la Order"
        });
    }
});
module.exports = router;