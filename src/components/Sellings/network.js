const express = require('express');
const router = express.Router();
const fetch = require('node-fetch');

let relacion = [
   {
         catOne: "Colchones",
         catTwo: "Juegos de alcoba",
         catTrhee: "Camas",
         catFour: "Base camas"
   },
   {
         catOne :"Juegos de alcoba",
         catTwo: "Colchones",
         catTrhee: "Closets y comodas",
         catFour: null
   },
   {
         catOne : "Camas ",
         catTwo : "Colchones",
         catTrhee: "Mesas de noche y tocadores",
         catFour: "Closets y comodas"

   },
   {
         catOne :"Base camas",
         catTwo: "Colchones",
         catTrhee: "Mesas de noche y tocadores",
         catFour: "cabeceros"
   },
   {
         catOne : "Camas duplex",
         catTwo: "Colchones",
         catTrhee: "Base camas",
         catFour: "Cunas y camas infantiles"
   },
    {
         catOne : "Cunas y camas infantiles",
         catTwo: "Infantil/juvenil",
         catTrhee: null,
         catFour: null
   },
   {
         catOne :"Mesas de noche y tocadores",
         catTwo: "Camas",
         catTrhee: "Base camas",
         catFour: "Camas duplex"
   },
   {
         catOne :"Closets y comodas",
         catTwo: "Camas",
         catTrhee: "Base camas",
         catFour: "Mesas de noche y tocadores"
   },
    {
         catOne: "Juegos de sala",
         catTwo: "Juegos de comedor",
         catTrhee: "Salas en L",
         catFour: "mesas de centro y auxiliares"
   },
   {
         catOne :"Salas en L",
         catTwo: "mesas de centro y auxiliares",
         catTrhee: "Juegos de sala",
         catFour: "Juegos de comedor"
    },
    {
         catOne : "Sofás",
         catTwo: "Sofacamas",
         catTrhee: "Sofareclinable",
         catFour: "Salas en L"
    },
    {
         catOne : "Sofacamas",
         catTwo: "Sofás",
         catTrhee: "Salas en L",
         catFour: "Sofareclinable"
    },
    {
         catOne :"Sofareclinable",
         catTwo: "Salas en L",
         catTrhee: "Sofás",
         catFour: "sillas butacas, poltronas y puff"
   },
   {
         catOne :"sillas butacas, poltronas y puff",
         catTwo: "Juegos de sala",
         catTrhee: "Salas en L",
         catFour: null
   },
   {
         catOne :"consolas, vitrinas y muebles bar",
         catTwo: "Juegos de sala",
         catTrhee: "Juegos de comedor",
         catFour: "Salas en L"
   },
   {
         catOne : "mesas de centro y auxiliares",
         catTwo: "Salas en L",
         catTrhee: "Sofás",
         catFour: "Sofacamas"
   },
   {
      catOne : "Juegos de comedor",
      catTwo: "Juegos de sala",
      catTrhee: "sillas butacas, poltronas y puff",
      catFour: "mesas de centro y auxiliares"
   },
   {
         catOne :"Mesas",
         catTwo: "Sillas, butacos y bancos centro de entretenimiento, y mesas de tv",
         catTrhee: null,
         catFour: null
   },
    {
         catOne :"Sillas, butacosybancos",
         catTwo: "Mesas",
         catTrhee: "Sofacamas",
         catFour: null
    },
    {
         catOne : "centro de entretenimiento, y mesas de tv",
         catTwo: "Sofás",
         catTrhee: "Salas en L",
         catFour: "sillas butacas, poltronas y puff"
    },
    {
         catOne: "Mecedoras",
         catTwo: "centro de entretenimiento, y mesas de tv",
         catTrhee: "Salas en L",
         catFour: "Sofás "
    },
    {
         catOne : "Bibliotecas, mesas de computoy sillas de oficina",
         catTwo: "centro de entretenimiento, y mesas de tv",
         catTrhee: "Sofacamas",
         catFour: "sillas butacas, poltronas y puff"
    },
    {
         catOne : "cabeceros",
         catTwo: "Base camas",
         catTrhee: null,
         catFour: null
    },
    {
         catOne :"Infantil",
         catTwo: "Colchonetas",
         catTrhee: "Otros productosde infantil",
         catFour: null
   }
];
const prodcutos = async (products, amount, segment) => {
   let productos = products[0];
   var datos = [];
   for (itens in productos) {
      if(productos[itens] != null){
         console.log(productos[itens]);
         let product = {
            "agency": "01",
            "tags": [
               {
                  "key": "level2_description",
                  "value": productos[itens],
               },
               {
                  "key" : "segment",
                  "value" : segment ? segment : "AZUL"
               }
            ],
            "range": [
                {
                    "key" : "regular_price",
                    "lte" :  amount ? amount : 1200000
                }
            ],
            "sort": {
                "key": "ranking",
                "order": "desc"
            },
            "size" : 1,
            "keys": ["sku"]
        };
         await fetch(`https://jfay6day2c.execute-api.us-east-1.amazonaws.com/dev/products`, {
            method: 'POST',
            headers: { 'x-api-key' : 'bgS2GS3FVk7sJmNWiTHNP8Ug57vvZepE6PRhbXfl' },
            body: JSON.stringify(product),
         }).then(res => res.json()).then(data => { datos.push(data) })
      }
   }
   return datos
}

router.post('/', async (req, res) => {
   const {product, amount, segment} = req.body;

   let myproduct = relacion.filter(iten => iten.catOne == product);
   let data = await prodcutos(myproduct, amount, segment);
    res.json({
        bien: myproduct[0],
        lista:  data
    });
});

module.exports = router;