const fetch = require('node-fetch');

function SearchNavegation (value){
    return new Promise( (resolve, reject) => {
        if(!value){
            console.error('[messageController] no hay usuario o mensaje');
            return reject('Datos incorrectos');
        }else{

            var query = {
                "requestIdentifierType": "PRIMARYIDENTIFIER",
                "identifierScope": "GLOBAL",
                "identifierType": "CC",
                "identifierValue": value
            }

           const datos =  fetch(`https://3kquh74ev6.execute-api.us-east-1.amazonaws.com/api/customerhistory`,{
                method: 'POST',
                headers: {
                    'x-api-key' : 'pSDco7u6pN22tNnZMWnj62kCSxBgp7CX2BhmjrEM' },
                body: JSON.stringify(query),
            }).then(res => res.json())
            .then(data => {
                var UserNavegatios = [];
                var elemts= [];
                var page = [];
                var itemsView = [];
                const navegacion = [];
                const costumer = [];
        
                data.map((item)=>{
                    if(item.customer){
                        costumer.push(item.customer);
                    }
                    if(item.interactions){
                        navegacion.push(item.interactions)
                    }
                })
//sacando elementos
                navegacion.map( ( item, index ) => {
                    item.map( (items, i )=> {
                        UserNavegatios.push(items)
                    })
                });
                UserNavegatios.map( (item) => {
                    if(item.eventDataPoint.eventData.items){

                        item.eventDataPoint.eventData.items.map( (iten) => {
                            itemsView.push(iten); 
                        });

                    }
                    if(item.eventDataPoint.eventData.page){
                        page.push(item.eventDataPoint.eventData.page);
                    }
                    if(item.eventDataPoint.eventData.element){
                        elemts.push(item.eventDataPoint.eventData.element);
                    }
                    
                })
                var userData = {
                    costumer,
                    ProductsView : itemsView,
                    pagesView: page,
                    categorys: elemts
                }
                return userData
            })
            return resolve(datos); 
        }
    })
};

module.exports = {
    SearchNavegation
}