const fetch = require('node-fetch');
const express = require('express');
const router = express.Router();

router.post('/', async (req, res) => {
    let { cc } = req.body;

    try{
        let payload = {
                "grant_type": "client_credentials",
                "client_id": "qmed1vx3ezf1uouywyk00i82",
                "client_secret": "CqxCXcjFiDNhHNZQOOO8GmqB",
                "account_id": "100015310"
            };
        let dataToken = await fetch(`https://mcd1pnbmqxyrgdmcw9dqddwsw244.auth.marketingcloudapis.com/v2/token`, {
            method: 'POST',
            headers : {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(payload),
        }).then(data => data.json()).then(data =>  data);

        let payloadTwo = {
            "items": [
                {
                    "N_IDE": cc,
                    "CLIENTE_REGISTRADO": "SI"
                }
            ]
        };
        let dataTokenUpdadate = await fetch(dataToken.rest_instance_url+'/data/v1/async/dataextensions/key:D59BB2CA-477D-488A-BDE3-2623B6481CC8/rows', {
            method: 'PUT',
            headers : {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${dataToken.access_token}`,
            },
            body: JSON.stringify(payloadTwo),
        }).then(data => data.json()).then(data => data);

        /* let statusItems = await fetch(dataToken.rest_instance_url+`/data/v1/async/${dataTokenUpdadate.requestId}/status`, {
            method: 'GET',
            headers : {
                'Authorization': `Bearer ${dataToken.access_token}`,
            }
        }).then(data => data.json())
        .then(data => {
            console.log(data)
            return data
        }); */

        res.json({
            state: true,
            data: dataTokenUpdadate,
           // status: statusItems
        });
    }
    catch(e){
        res.json({
            state: false
        });
    }
});

module.exports = router;