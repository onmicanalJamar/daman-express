const express = require('express');
const router = express.Router();
const fetch = require('node-fetch');

const searchProduct = async (product, price) =>{

    let variant = {
        "agency": "01",
        "tags": [ { 
            "key": "name",
            "value": product 
        } ],
        "range": [
            {
                "key" : "regular_price",
                "lte" : price
            }
        ],
        "size" : 5,
        "exists": ["ff_price", "img", "availability","segment"],
        "keys": ["sku", "name","regular_price" ,"ff_price","ff_price_base","segment", "url", "price_base", "availability", "img", "shopifyData", "step_info.name"]
    };
    const data = await fetch(`https://jfay6day2c.execute-api.us-east-1.amazonaws.com/dev/products`, {
        method: 'POST',
        headers: {
            'x-api-key' : 'bgS2GS3FVk7sJmNWiTHNP8Ug57vvZepE6PRhbXfl',
        },
        body: JSON.stringify(variant),
    }).then(res => res.json()).then(data => data).catch(err => err)
    return data
}
router.post('/', async (req, res) => {
    let { product , price }= req.body;
    const productoRecomendados= await searchProduct(product, price)

    res.json({
        "variantes" : productoRecomendados,
    });
});

module.exports = router;