const express = require('express');
const router = express.Router();
const fetch = require('node-fetch');

Array.prototype.unique=function(a){
    return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
});

const opSearch = async( cedula, evento, size ) => {
    let datos = {
        "customer": {
            "primaryIdentifier": {
                "scope": "GLOBAL",
                "type": "CC",
                "value": cedula
            }
        },
        "filter": {
            "eventClassifier": {
                "type": ["CATEGORYVIEWS" ],
                 "orderByDate": "DESC"
            }
        },
        "identifiers": ["MD"]
    };
    const informa = await fetch(`https://z62u1423sb.execute-api.us-east-1.amazonaws.com/v1/customerevents`, {
           method: 'POST',
           headers: { 'x-api-key' : 'pSDco7u6pN22tNnZMWnj62kCSxBgp7CX2BhmjrEM' },
           body: JSON.stringify(datos),
    }).then(res => res.json()).then(data => {
          if(data.found == true){
              var categorias =[];
              var dataCustomer = []
              let info = data.customers[0].events;
              let infoCustomer = data.customers[0].customer.identifiers[0].attributes;
              info.map( i => {
                categorias.push(i.event.element.category)
              })
              infoCustomer.map( i => {

                  if(i.attributeName == "PRODUCT_CATEGORY"){
                    i.attributeValue.map( car => categorias.push(car.toLowerCase() ))
                  }else{
                    var infoCLient ={
                        key: i.attributeName,
                        value: i.attributeValue
                    }
                    dataCustomer.push(infoCLient);
                  }
              });
              let resultCategoris =categorias.unique();
            return { categorias: resultCategoris, dataCustomer}
          } else{ return [] }
    }).catch( (err) => {
          return err
    });
    return informa
}
const SuperAsesor = async( cedula, evento, size ) => {
    let datos = {
        "customer": {
            "primaryIdentifier": {
                "scope": "GLOBAL",
                "type": "CC",
                "value": cedula
            }
        },
        "filter": {
            "eventClassifier": {
                "type": ["CATEGORYVIEWS" ],
                 "orderByDate": "DESC"
            }
        },
        "identifiers": [ "MD" ]
    };
    const informa = await fetch(`https://z62u1423sb.execute-api.us-east-1.amazonaws.com/v1/customerevents`, {
           method: 'POST',
           headers: { 'x-api-key' : 'pSDco7u6pN22tNnZMWnj62kCSxBgp7CX2BhmjrEM' },
           body: JSON.stringify(datos),
    }).then(res => res.json()).then(data => {
        if(data.found == true){
            var categorias =[];
            let info = data.customers[0].events;
            info.map( i => {
            categorias.push(i.event.element.category)
            })
        return categorias
        } else{ return [] }
    }).catch( (err) => {
        return err
    });
    return informa
}
const searchProduct = async (category, segment) => {
   let data  = {
        "agency": "01",
        "tags": [
            {
                "value":category
            },
            {
				"key" : "segment",
				"value" : segment ? segment : "AMARILLO"
			}
        ],
        "sort": {
            "key" : "ranking",
            "order" : "desc"
        },
        "exists": ["ff_price", "img", "availability","segment"],
        "keys": [ "suggested", "img", "ff_price_base", "segment","regular_price", "sku", "name", "img", "imgs","ranking" ,"shopifyData", "step_info.name"]
    }
    const dat = await fetch('https://jfay6day2c.execute-api.us-east-1.amazonaws.com/dev/products', {
        method: 'POST',
        headers: { 'x-api-key' : 'bgS2GS3FVk7sJmNWiTHNP8Ug57vvZepE6PRhbXfl' , "Content-Type": "application/json"},
        body: JSON.stringify(data),
    }).then(data => data.json()).then(data => data)
    return dat
};

router.post('/', async (req, res) => {

    const { cc , event, size} = req.body;
    let info = await opSearch(cc,event, size);
    var pdoductos = [];

     if(info.categorias !== undefined){
         var categori = info.categorias;
         for(let i = 0; i <= categori.length -1; i++){
            let daprd = await searchProduct(categori[i]);
            let prd = {
                category: categori[i],
                products: daprd
            }
            pdoductos.push(prd);
         }
        res.json({
            categoryView: info,
            products: pdoductos
        })
    }else{
        res.json({
            categoryView: [],
        })
    }
});

module.exports = router;