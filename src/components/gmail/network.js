const express = require('express');
const router = express.Router();
const { success, error } = require('../../responses');

const { SearchNavegation } = require('./controller');

router.post('/', (req, res) => {
    const { value } = req.body;

    SearchNavegation(value)
        .then( (fullMessage) =>  success(req, res, fullMessage, 201 , 'god') )
        .catch( err => error(req ,res , 'Unexpected Error', 400, err) )
});
module.exports = router;