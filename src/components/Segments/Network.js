const express = require('express');
const router = express.Router();
const fetch = require('node-fetch');



const searchClient = async (cc, color) => {
    let logica = {
        "size": 0,
        "query": {
            "bool": {
                "must": [
                    {
                        "has_child": {
                            "type": "event",
                            "query": {
                                "bool": {
                                    "must": [
                                        {
                                            "match": {
                                                "event.businessInteractionTerminal.device": "APPVENDEDOR"
                                            }
                                        },
                                        {
                                            "match": {
                                                "event.eventDataPoint.eventData.informacion_de_vendedor.object.n_ide.string": cc
                                            }
                                        },
                                        {
                                            "match": {
                                                "event.eventDataPoint.eventData.informacion_de_cliente.object.informacion.object.estrategia.string": color
                                            }
                                        }
                                    ]
                                }
                            }
                        }
                    },
                    {
                        "has_child": {
                            "type": "identifier",
                            "query": {
                                "bool": {
                                    "must": [
                                        {
                                            "match": {
                                                "identifier.identifierType": "CC"
                                            }
                                        }
                                    ]
                                }
                            }
                        }
                    }
                ]
            }
        },
        "aggs": {
            "group_by_customer": {
                "terms": {
                    "field": "cii",
                    "size": 20
                }
            }
        }
    };
    const datos = await fetch('https://2cfmxhyhph.execute-api.us-east-1.amazonaws.com/v1/_search', {
        method: 'POST',
        headers: {
            "x-api-key" : "bgS2GS3FVk7sJmNWiTHNP8Ug57vvZepE6PRhbXfl",
            "Content-Type": "application/json"
        },
        body: JSON.stringify(logica),
        }).then(res => res.json()).then( async (data) => {

             return ({ 
                segment: color, 
                clientList: data.aggregations.group_by_customer.buckets,
                count: data.aggregations.group_by_customer.buckets.length
            })
        }).catch(err => err)
    return datos
};

router.post('/', async (req, res) => {
    let { cc }= req.body;

    const colors = [ "AMARILLO" , "AZUL" , "NARANJA"];
    var client = [];
    for(let color of colors){
        let data = await searchClient(cc, color)
        client.push(data)
    }

    if(client.length >= 1){
        res.json({
            api : "Segement",
            state: true,
            body: client
        });
    } 
});

module.exports = router;