const connectDB = require('./store');

function ProductView (query){
    return new Promise( async (resolve, reject) => {
        if(!query.eventId){
            console.error('[messageController] no hay usuario o mensaje');
            return reject('Datos incorrectos');
        }else{
            let db , op;
            db = await connectDB();
            const Product = query;
            op = await db.collection('ProductsView').insertOne(Product);

            return resolve(op);
        }
    })
};
function LastProductView(query){
    return new Promise( async (resolve, reject) => {
        try {
            let db , op;
            db = await connectDB();
            op = await db.collection('ProductsView').find().limit(1).toArray()
            return resolve(op);
        } catch (error) {
            return reject('no hay datos');
        }
    })
};
module.exports = {
    ProductView,
    LastProductView
}