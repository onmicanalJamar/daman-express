
const MongoClient = require('mongodb').MongoClient

const mongoUrl = `mongodb+srv://omnicanalAdmin:Jamar2021@interactions-kjzer.mongodb.net/test?retryWrites=true&w=majority`;

let connection

async function connectDB(){
    if(connection) return connection

    let client

    try {
        client = await MongoClient.connect(mongoUrl, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
        connection = client.db("Interactions")
    } catch (error) {
        console.log('No se pudo conectar a la base de datos de mongo', mongoUrl, error)
        process.exit(1)
    }
    return connection
}
module.exports = connectDB;