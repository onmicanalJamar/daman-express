const OpCtrl = {};
const OpModels = require('./store');
const connectDB = require('./store');
function Opfunction (query){
    return new Promise( async (resolve, reject) => {
        if(!query.eventId){
            console.error('[messageController] no hay usuario o mensaje');
            return reject('Datos incorrectos');
        }else{
            let db , op;
            db = await connectDB();
            const newOP = query;
            op = await db.collection('OpCollection').insertOne(newOP);

            return resolve(op);
        }
    })
};
function LastOpCustomers (query){
    return new Promise( async (resolve, reject) => {
        try {
            let db , op;
            db = await connectDB();
            op = await db.collection('OpCollection').find().limit(1).toArray()
            return resolve(op);
        } catch (error) {
            return reject('no hay datos');
        }
    })
};
module.exports = {
    Opfunction,
    LastOpCustomers
}