const express = require('express');
const router = express.Router();
const fetch = require('node-fetch');
//process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';

const getDataUSer =  async (cedula,CII ,money) => {

    let datos = {};
    if(cedula){
        datos = {
            cc: cedula
        }
    }else{
        datos = {
            cii: CII
        }
    }
    var sataUSe = await fetch(`https://cdp.jamar.co/api/v1/relations`, {
        method: 'POST',
        headers: { 'Content-Type' : 'application/json' },
        body: JSON.stringify(datos),
    }).then(res => res.json()).then(data => {
       return data
    }).catch( (err) => {
       return err
    });
    return  sataUSe
}
const productView = async (cedula, CII ,money,) =>{
    let datos = {
        "customer": {
            "primaryIdentifier": {
              "scope": "GLOBAL",
              "type" : cedula ? "CC" : "CII",
              "value": cedula ? cedula : CII
            }
          },
          "filter": {
            "eventClassifier":{
                "type": ["PRODUCTSVIEW"],
                "orderByDate": "ASC"
            },
            "limit" : 10
          }
    }
    var petitions = await fetch(`https://z62u1423sb.execute-api.us-east-1.amazonaws.com/v1/customerevents`, {
        method: 'POST',
        headers: { "x-api-key":"pSDco7u6pN22tNnZMWnj62kCSxBgp7CX2BhmjrEM"},
        body: JSON.stringify(datos),
    }).then(res => res.json()).then(data => {
        var productosVistos = [];
        if(data.found == true){
            data.customers[0].events.map(i=> {
                productosVistos.push(i.event.items[0])
            });
            return productosVistos
        }else{
            return []
        } 
    }).catch( (err) => {
       return err
    });
    return petitions
}
const searchProduct = async (color, monto, category) =>{
    let product = {};
    if(category){
        product = {
            "agency": "01",
            "tags": [
                    {
                        "value": category
                    },
                    {
                        "key" : "segment",
                        "value" : color ? color : "AMARILLO"
                    }
                ],
            "range": [
                {
                    "key" : "regular_price",
                    "lte" : monto ? monto : 2000000
                }
            ],
            "sort": {
                "key": "ranking",
                "order": "desc"
            },
            "size" : 10,
            "exists": ["ff_price", "img", "availability","segment"],
            "keys": ["sku"]
        };
    }else{
        product = {
            "agency": "01",
            "tags": [
                    {
                        "key" : "segment",
                        "value" : color ? color : "AMARILLO"
                    }
                ],
            "range": [
                {
                    "key" : "regular_price",
                    "lte" : monto ? monto : 2000000
                }
            ],
            "sort": {
                "key": "ranking",
                "order": "desc"
            },
            "size" : 10,
            "exists": ["ff_price", "img", "availability","segment"],
            "keys": ["sku"]
        }; 
    }
    
    const data = await fetch(`https://jfay6day2c.execute-api.us-east-1.amazonaws.com/dev/products`, {
        method: 'POST',
        headers: {
            'x-api-key' : 'bgS2GS3FVk7sJmNWiTHNP8Ug57vvZepE6PRhbXfl' ,
            "Content-Type": "application/json"
        },
        body: JSON.stringify(product),
    }).then(res => res.json()).then(data => data).catch(err => err)
    return data
}

router.post('/', async (req, res) => {
    let { cc ,cii, amount, segment , size , category }= req.body;
    const carritos =  await getDataUSer(cc,cii ,size);
    const productsView = await productView(cc, cii);
    const productoRecomendados = await searchProduct(segment, amount, category)

    res.json({
        carritos,
        "productView" : productsView,
        "recomendados" : productoRecomendados,
    });
});

module.exports = router;
