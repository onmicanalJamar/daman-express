const { GoogleSpreadsheet } = require('google-spreadsheet');
const { promisify } = require('util');
const credencialtes = require('./credentials.json');
const fetch = require('node-fetch');
const express = require('express');
const router = express.Router();

const productShopifyJamar = async (sku, region) => {
    let product = fetch(`https://www.jamar.com/search?type=product&q=variants.sku:${sku}&view=sku`)
    .then(data => data.json()).then(data => data);
    return product
};
const getCuota = async (sku, region) => {
    let cuota = fetch(`https://ihoffgzow6.execute-api.us-east-1.amazonaws.com/api/v1/feesProducts/JA/${region ? region :"01"}?sku='${sku}'&tven='OR'&tipoGrupo='A','G'`)
        .then(data => data.json()).then(data => data);
    return cuota
}

router.get('/', async (req, res) => {

    try{
        const doc = new GoogleSpreadsheet('1L8GSuMXfWGFhjo5zzVMmRLXgYuKTp0VA9F3GCBPQpAA');
        await doc.useServiceAccountAuth(credencialtes);

        await doc.loadInfo();
        const sheet = doc.sheetsByIndex[0];

        const rows = await sheet.getRows();

        let social = [];
        let dormitorio = [];
        let estudio = []
        let comedores = [];
        let colchones = []

        rows.forEach((i) => {
            if(i.Lin == "SALAS"){
                let obj = {
                    sku: i.CODIGO,
                    product: i.PRODUCTO,
                    cuota16: i.M16 ? element.M16 : "0",
                    cuota40: i.M40 ? element.M40 : "0",
                    cat: i.cat,
                    lin: i.Lin,
                    sublinea: i.Sublinea
                }
                social.push(obj);
            }
            if(i.Lin == "DORMITORIO"){
                let obj = {
                    sku: i.CODIGO,
                    product: i.PRODUCTO,
                    cuota16: i.M16 ? element.M16 : "0",
                    cuota40: i.M40 ? element.M40 : "0",
                    cat: i.cat,
                    lin: i.Lin,
                    sublinea: i.Sublinea
                }
                dormitorio.push(obj)
            }
            if(i.Lin == "DESCANSO"){
                let obj = {
                    sku: i.CODIGO,
                    product: i.PRODUCTO,
                    cuota16: i.M16 ? element.M16 : "0",
                    cuota40: i.M40 ? element.M40 : "0",
                    cat: i.cat,
                    lin: i.Lin,
                    sublinea: i.Sublinea
                }
                estudio.push(obj);
            }
            if(i.Lin == "COMEDORES"){
                let obj = {
                    sku: i.CODIGO,
                    product: i.PRODUCTO,
                    cuota16: i.M16 ? element.M16 : "0",
                    cuota40: i.M40 ? element.M40 : "0",
                    cat: i.cat,
                    lin: i.Lin,
                    sublinea: i.Sublinea
                }
                comedores.push(obj);
            }
            if(i.Lin == "COLCHONES"){
                let obj = {
                    sku: i.CODIGO,
                    product: i.PRODUCTO,
                    cuota16: i.M16 ? element.M16 : "0",
                    cuota40: i.M40 ? element.M40 : "0",
                    cat: i.cat,
                    lin: i.Lin,
                    sublinea: i.Sublinea
                }
                colchones.push(obj);
            }
        })
        let finalProducto= [];
        finalProducto.push(
            {type: "salas", prd: social },
            {type: "dormitorio", prd:  dormitorio },
            {type: "DESCANSO", prd: estudio },
            {type: "comedores", prd: comedores },
            {type: "colchones", prd: colchones}
        );
/*         function infoGEt (){
            finalArray.forEach((i) => {
                let item = []
                i.forEach( async (element) => {

                    if(element.CODIGO != undefined){
                       // console.log(element.cat)
                        let obj = {
                            sku: element.CODIGO,
                            product: element.PRODUCTO,
                            cuota16: element.M16 ? element.M16 : "0",
                            cuota40: element.M40 ? element.M40 : "0"
                        }
                        item.push(obj);
                    }
                });
                finalProducto.push(item);
            });

        }
        infoGEt() */;

        //3057039097

        res.json({
            state: true,
            data: finalProducto
        });
    }
    catch(e){
        console.log(e);
    }
});

module.exports = router;
