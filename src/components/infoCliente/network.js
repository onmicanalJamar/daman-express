const express = require('express');
const router = express.Router();
const fetch = require('node-fetch');


const infoClinet = async (CII) => {
    let data = {
        "customer": {
            "primaryIdentifier": {
                "scope": "GLOBAL",
                "type" : "CII",
                "value": CII
                }
            },
        "identifiers": ["PID", "MD"],
        "filter": {  
            "limit": 0
        }
    }
    const datos = await fetch('https://z62u1423sb.execute-api.us-east-1.amazonaws.com/v1/customerevents', {
        method: 'POST',
        headers: {
            "x-api-key" : "bgS2GS3FVk7sJmNWiTHNP8Ug57vvZepE6PRhbXfl",
            "Content-Type": "application/json"
        },
        body: JSON.stringify(data),
    }).then(res => res.json()).then(data => {

        let price = "";
        let estrategia = "";
        let categoty = "";
        let cedula =  "";
        data.customers[0].customer.identifiers.map(i => {
            if(i.identifierType == "CC"){
                cedula = i.identifierValue;
            }
            if(i.attributes != undefined){
                i.attributes.map(iten => {
                    if(iten.attributeName == "MAXQUOTA"){
                        price = iten.attributeValue;
                    }
                    if(iten.attributeName == "STRATEGY"){
                        estrategia = iten.attributeValue;
                    }
                    if(iten.attributeName == "PRODUCT_CATEGORY"){
                        categoty = iten.attributeValue[0];
                    }
                })
            }
        });

        return { data: {
            cc: cedula,
            estrategia: estrategia,
            categoty: categoty.toLowerCase(),
            monto: price
        }}
        
    }).catch(err => err);
    return datos
};

const getDataClient = async (data) => {
    let {cc, estrategia, categoty, monto} = data.data;

    let dataNueva = {};

    if(categoty != ''){
        dataNueva = {
            "cc": cc,
            "size" : 10,
            "amount" : monto, 
            "segment" : estrategia,
            "category": categoty ? categoty : "comedores"
        };
    }else{
        dataNueva = {
            "cc": cc,
            "size" : 10,
            "amount" : monto, 
            "segment" : estrategia
        };
    }
    

    const datos = await fetch('https://cdp.jamar.co/api/v1/suggestion', {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(dataNueva),
    }).then(data => data.json()).then(data => data)
    return datos
}
router.post('/', async (req, res) => {
    let { cii }= req.body;

    let data = await infoClinet(cii);
    let datos = await getDataClient(data);
    res.json({
        state: true,
        body: {
            data,
            recomendado: datos
        },
    });
});

module.exports = router;