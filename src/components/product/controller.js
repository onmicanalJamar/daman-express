const fetch = require('node-fetch');

function productSearch (query){
    return new Promise( (resolve, reject) => {
        if(!query){
            console.error('[messageController] no hay usuario o mensaje');
            return reject('Datos incorrectos');
        }else{
            const datos = fetch(`https://jfay6day2c.execute-api.us-east-1.amazonaws.com/dev/products`, {
                method: 'POST',
                headers: { 'x-api-key' : 'bgS2GS3FVk7sJmNWiTHNP8Ug57vvZepE6PRhbXfl' },
                body: JSON.stringify(query),
            }).then(res => res.json()).then(data => data)
            .catch( (err) => {
                res.json(err)
            });
            return resolve(datos);
        }
    })
};

module.exports = {
    productSearch
}