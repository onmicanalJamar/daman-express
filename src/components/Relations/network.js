const express = require('express');
const router = express.Router();
const fetch = require('node-fetch');

const opSearch = async( cedula, cii ,size ) => {
    let datos = {
        "customer": {
        "primaryIdentifier": {
            "scope": "GLOBAL",
            "type" : cedula ? "CC" : "CII",
            "value": cedula ? cedula : cii
        }
        },
        "filter": {
            "eventClassifier": {
            "type": ["ORDER_GENERATION"]
        },
        "limit" : size ? size : 10,
        "keys": ["purchaseOrder.seller.identification", "purchaseOrder.header.FEC_HORA" ,"purchaseOrder.agency", "purchaseOrder.sellType" ,"purchaseOrder.items[0].COD"]
        }
    }

   const getProduct = async (sku) => {
        let poduct = {
        "agency": "01",
        "tags": [
                {
                    "key": "sku",
                    "value": sku
                }
            ],
            "size" : 1,
            "keys": ["sku"]
        }
        const data = await fetch(`https://jfay6day2c.execute-api.us-east-1.amazonaws.com/dev/products`, {
            method: 'POST',
            headers: {
                'x-api-key' : 'bgS2GS3FVk7sJmNWiTHNP8Ug57vvZepE6PRhbXfl' ,
                "Content-Type": "application/json"
            },
            body: JSON.stringify(poduct),
        }).then(res => res.json()).then(data => data).catch(err => err)
        return data
    }
    const informa = await fetch(`https://z62u1423sb.execute-api.us-east-1.amazonaws.com/v1/customerevents`, {
         method: 'POST',
         headers: { 'x-api-key' : 'pSDco7u6pN22tNnZMWnj62kCSxBgp7CX2BhmjrEM' },
         body: JSON.stringify(datos),
    }).then(res => res.json()).then(data => {
        let validate = data.found;
        var datosOp = [];
        if(validate == true) {
            eventsData.map( async(i)=> {

                let prd = i.event.purchaseOrder.items[0].COD;
                let popData = {
                    dataOp: i.event.purchaseOrder,
                }

                datosOp.push(popData)
            });
            return datosOp
        }else{
            return []
        }

     }).catch( (err) => {
        return err
     });
    return informa
}
const CardAbandonado = async (cedula, cii, size) => {
    let datos = {
        "customer": {
          "primaryIdentifier": {
            "scope": "GLOBAL",
            "type" :  cedula ? "CC" : "CII",
            "value": cedula ? cedula : cii
          }
        },
        "filter": {
            "eventClassifier": {
            "type": ["ADDCARTS"],
            "orderByDate": "DESC"
          },
          "limit" : size ? size : 10,
         // "keys": [""]
        }
    }
    let datosTwo = {
        "customer": {
            "primaryIdentifier": {
                "scope": "GLOBAL",
                "type" :  cedula ? "CC" : "CII",
                "value": cedula ? cedula : cii
            }
        },
        "filter": {
            "eventClassifier": {
            "type": ["PURCHASE"],
            "orderByDate": "DESC"
          },
          "limit" : size ? size : 10
        //  "keys": ["purchaseOrder.seller.identification", "purchaseOrder.header" ,"purchaseOrder.agency", "purchaseOrder.sellType" ,"purchaseOrder.items"]
        }
    }
    const compra = await fetch(`https://z62u1423sb.execute-api.us-east-1.amazonaws.com/v1/customerevents`, {
        method: 'POST',
        headers: { 'x-api-key' : 'pSDco7u6pN22tNnZMWnj62kCSxBgp7CX2BhmjrEM' },
        body: JSON.stringify(datosTwo)
    }).then(resp => resp.json()).then(data => {
        let infor = data.customers[0] ? data.customers[0].events :[];
        return infor
    });

    const addCars = await fetch(`https://z62u1423sb.execute-api.us-east-1.amazonaws.com/v1/customerevents`, {
        method: 'POST',
        headers: { 'x-api-key' : 'pSDco7u6pN22tNnZMWnj62kCSxBgp7CX2BhmjrEM' },
        body: JSON.stringify(datos),
    }).then(res => res.json()).then( (data) => {
        let validation = data.found;

        if(validation == true){
            let eventsData = data.customers[0].events;
            var opdat = [];
            eventsData.map( i => {
                opdat.push(i.event)
            });
            return opdat
        }else{
            return []
        }
    }).catch( (err) => {
       return err
    });

    let info = {cars: addCars, comprado: compra }
    return info
};

router.post('/', async (req, res) => {

    const { cc , cii , size} = req.body;
    let info = await opSearch(cc, cii, size);
    let carros = await CardAbandonado(cc, cii, size);
    var productoTodos = [];
    var compradosProduc = [];
    var carritoProduc = [];

    if(info.length >= 1){
        let skuOp = info[0].dataOp.items[0].COD;
        productoTodos.push(skuOp)
    }
    if(carros.cars.length >=1){
        carritoProduc = carros.cars;
        carritoProduc.map( produc => {
            productoTodos.push(produc.items[0])
        } )
    }
    if(carros.comprado>=1){
        compradosProduc = carros.comprado;
        compradosProduc.map( i => {
            i.event.event.line_items.map( iten => {
                productoTodos.push(iten)
            } )
        });
    }

    let compare = "";
    var otherProduct = [];
    productoTodos.map( i => {
        if(  typeof i === 'string' ){
            compare = i
        }
        else{
            otherProduct.push(i)
        }
    });
    var otris = otherProduct.filter( ite => ite.sku !== compare );

    res.json({
        carritoAbandonado: otris
    });

});
module.exports = router;