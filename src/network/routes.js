const express = require('express');
const bodyParser =  require("body-parser");

const cookies = require('../components/cookies/network');
const cc = require('../components/cc/network');
const gmail = require('../components/gmail/network');
const product = require('../components/product/network');
const relations = require('../components/Relations/network');
const categorys = require('../components/categorys/network');
const suggestion = require('../components/Suggestion/network');
const variantes = require('../components/variantes/network');
const selling = require('../components/Sellings/network');
const visor = require('../components/Visor/Network');
const segments = require('../components/Segments/Network');
const infoClient = require('../components/infoCliente/network');
const pago = require('../components/pago/network');
const createOP = require('../components/pago/createOP');
const guarCheck = require('../components/pago/guardarCarrito');
const allOrder = require('../components/pago/orderList');
const searchClient = require('../components/pago/searchClient');
const emails = require('../components/email');
const crediProduct = require('../components/realTimeProduct');
const SearchORdenShopify = require('../components/srchTokens');
const RegisteSalesFoce = require('../components/Salesforce/Update');

const routes = (server) => {
    server.use('/api/userdate/navegation/cookies', bodyParser.urlencoded({ extended: true }), cookies);
    server.use('/api/userdate/navegation/cc',bodyParser.urlencoded({ extended: true }) ,cc);
    server.use('/api/userdate/navegation/gmail',bodyParser.urlencoded({ extended: true }) ,gmail);
    server.use('/api/product', bodyParser.urlencoded({ extended: true }),product);
    server.use('/api/v1/relations', bodyParser.urlencoded({ extended: true }) ,relations);
    server.use('/api/v1/categories', bodyParser.urlencoded({ extended: true }) ,categorys);
    server.use('/api/v1/suggestion',bodyParser.urlencoded({ extended: true }) ,suggestion);
    server.use('/api/v1/variantes', bodyParser.urlencoded({ extended: true }) ,variantes);
    server.use('/api/v1/selling',bodyParser.urlencoded({ extended: true }) ,selling);
    server.use('/api/v1/visorApp',bodyParser.urlencoded({ extended: true }) ,visor);
    server.use('/api/v1/Segments',bodyParser.urlencoded({ extended: true }) ,segments);
    server.use('/api/v1/dataUser', bodyParser.urlencoded({ extended: true }),infoClient);
    server.use('/api/v1/pago',bodyParser.urlencoded({ extended: true }) ,pago);
    server.use('/webhooks/orders', bodyParser.raw({ type: 'application/json' }) ,guarCheck);
    server.use('/api/v1/allOrders', bodyParser.urlencoded({ extended: true }) , allOrder);
    server.use('/api/v1/createop', bodyParser.urlencoded({ extended: true }),createOP);
    server.use('/api/v1/search/client', bodyParser.urlencoded({ extended: true }), searchClient);
    server.use('/api/v1/email', bodyParser.urlencoded({ extended: true }), emails);
    server.use('/api/v1/crediJamarProduct', bodyParser.urlencoded({ extended: true }), crediProduct);
    server.use('/api/v1/note', bodyParser.urlencoded({ extended: true }), SearchORdenShopify);
    server.use('/api/v1/registerdata', RegisteSalesFoce)
}

module.exports = routes;