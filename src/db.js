const { MongoClient } =require('mongodb'); 

const mongoUrl = `mongodb+srv://admindDev:Jean123456789@cluster0-lyuwv.mongodb.net/test?retryWrites=true&w=majority`;
let connection;

 const connectDB = async () => {
    if(connection) return connection

    let client

    try {
        client = await MongoClient.connect(mongoUrl, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
        connection = client.db("OPShopify")
    } catch (error) {
        console.log('No se pudo conectar a la base de datos de mongo', mongoUrl, error)
        process.exit(1)
    }
    return connection
}
module.exports = connectDB;